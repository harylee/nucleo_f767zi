#include <stdio.h>
#include "los_config.h"
#include "los_interrupt.h"
#include "los_task.h"

UINT32 g_taskExcId;
#define TSK_PRIOR 4

/* 模拟异常函数 */
	
UINT32 Get_Result_Exception_0(UINT16 dividend){
    UINT32 divisor = 0;
    UINT32 result = dividend / divisor;
    return result;
}

UINT32 Get_Result_Exception_1(UINT16 dividend){
    return Get_Result_Exception_0(dividend);
}

UINT32 Get_Result_Exception_2(UINT16 dividend){
    return Get_Result_Exception_1(dividend);
}

UINT32 Example_Exc(VOID)
{
    UINT32 ret;

    printf("Enter Example_Exc Handler.\r\n");

    /* 模拟函数调用 */
    ret = Get_Result_Exception_2(TSK_PRIOR);
    printf("Divided result =%u.\r\n", ret);

    printf("Exit Example_Exc Handler.\r\n");
    return ret;
}


/* 任务测试入口函数，创建一个会发生异常的任务 */
UINT32 Example_Exc_Entry(VOID)
{
    UINT32 ret;
    TSK_INIT_PARAM_S initParam;

    /* 锁任务调度，防止新创建的任务比本任务高而发生调度 */
    LOS_TaskLock();

    printf("LOS_TaskLock() Success!\r\n");

    initParam.pfnTaskEntry = (TSK_ENTRY_FUNC)Example_Exc;
    initParam.usTaskPrio = TSK_PRIOR;
    initParam.pcName = "Example_Exc";
    initParam.uwStackSize = LOSCFG_SECURE_STACK_DEFAULT_SIZE;
    /* 创建高优先级任务，由于锁任务调度，任务创建成功后不会马上执行 */
    ret = LOS_TaskCreate(&g_taskExcId, &initParam);
    if (ret != LOS_OK) {
        LOS_TaskUnlock();

        printf("Example_Exc create Failed!\r\n");
        return LOS_NOK;
    }

    printf("Example_Exc create Success!\r\n");

    /* 解锁任务调度，此时会发生任务调度，执行就绪队列中最高优先级任务 */
    LOS_TaskUnlock();

    return LOS_OK;
}