# 鸿蒙轻内核LiteOS-M移植支持Nucleo-F767Zi开发板

#### 介绍
该仓包含鸿蒙轻内核LiteOS-M移植支持`Nucleo-F767Zi`开发板的工程代码，支持`Ninja、GCC、IAR`等方式进行编译。

#### 一、Ninja编译

1.  获取[OpenHarmony](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/%E6%BA%90%E7%A0%81%E8%8E%B7%E5%8F%96.md)完整仓代码，建议使用`获取方式3：从代码仓库获取`方法获取代码。假设检出目录为`~/openHarmony`。

2.  参考[快速入门](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Readme-CN.md)搭建环境。对于LiteOS-M内核，按[搭建系统基础环境](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/%E6%90%AD%E5%BB%BA%E7%B3%BB%E7%BB%9F%E5%9F%BA%E7%A1%80%E7%8E%AF%E5%A2%83.md)配置环境即可。

3. 在本地目录，执行 `git clone https://gitee.com/harylee/nucleo_f767zi.git` 检出代码，假设检出目录为`~/nucleo_f767zi`。 执行如下命令把检出工程的`device`、`vendor`目录复制到`openHarmony`工程的相应目录。

```shell
cp -r ~/nucleo_f767zi/device/st  ~/openHarmony/device/st

cp -r ~/nucleo_f767zi/vendor/st  ~/openHarmony/vendor/st

chmod +x ~/openHarmony/device/st/nucleo_f767zi/build.sh
```

5.  下载arm交叉编译器，`git clone https://gitee.com/harylee/gcc-arm-none-eabi-10-2020-q4-major.git` ，将交叉编译器环境变量`bin`目录配置到`.bashrc`文件中或者配置`device/st/nucleo_f767zi/liteos_m/config.gni`文件中`board_toolchain_path`宏为交叉编译器`bin`路径。

6.  在OpenHarmony根目录：

```
haryslee@dev:~/OpenHarmony$ hb set
[OHOS INFO] Input code path:          # 直接按回车，然后选择nucleo_f767zi产品即可
OHOS Which product do you need?  nucleo_f767zi@st
haryslee@dev:~/OpenHarmony$ hb build
```
7.  最终的镜像生成在`~/openHarmony/out/nucleo_f767zi`/目录中，通过STM32 ST-LINK Utility软件将镜像文件下载至单板即可。

#### 二、GCC编译

需要确保已经安装好`make`和`arm gcc`。

1. 执行命令`git clone https://gitee.com/openharmony/kernel_liteos_m.git` 检出`kernel-liteos-m`代码。假设检出目录为`~/kernel-liteos-m`。

2. 执行下述命令检出三方库，假设检出目录为`~/kernel-liteos-m`。

```shell
mkdir ~/third_party

git clone https://gitee.com/openharmony/third_party_bounds_checking_function.git ~/third_party/bounds_checking_function

git clone https://gitee.com/openharmony/third_party_cmsis.git ~/third_party/cmsis

git clone https://gitee.com/openharmony/third_party_musl.git ~/third_party/musl
```

3. 在本地目录，执行 `git clone https://gitee.com/harylee/nucleo_f767zi.git` 检出代码，假设检出目录为`~/nucleo_f767zi`。 执行如下命令把检出工程的、`cortex-m7_nucleo_f767zi_gcc`目录复制到`kernel-liteos-m`工程的`targets`目录。

```shell
cp -r ~/nucleo_f767zi/cortex-m7_nucleo_f767zi_gcc  ~/kernel-liteos-m/targets/cortex-m7_nucleo_f767zi_gcc
```

4. 打开终端，切换到目录`~/kernel-liteos-m/targets/cortex-m7_nucleo_f767zi_gcc`，执行`make clean; make -j`即可启动编译。

5. 烧录、运行类似第一小节的`ninja`部分。

#### 三、IAR编译

需要确保已经安装好`IAR Embedded Workbench`。

1. 执行命令`git clone https://gitee.com/openharmony/kernel_liteos_m.git` 检出`kernel-liteos-m`代码。假设检出目录为`~/kernel-liteos-m`。

2. 在本地目录，执行 `git clone https://gitee.com/harylee/nucleo_f767zi.git` 检出代码，假设检出目录为`~/nucleo_f767zi`。 执行如下命令把检出工程的、`cortex-cortex-m7_nucleo_f767zi_iar`目录复制到`kernel-liteos-m`工程的`targets`目录。

```shell
cp -r ~/nucleo_f767zi/cortex-m7_nucleo_f767zi_iar  ~/kernel-liteos-m/targets/cortex-m7_nucleo_f767zi_iar
```

3. 切换到目录`~/kernel-liteos-m/targets/cortex-m7_nucleo_f767zi_iar/EWARM`，双击文件`Project.eww`，打开IAR软件。即可开始编译、烧录、调测运行。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

